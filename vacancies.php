<?php

$date_today = "2015-03-26";
$novosib = "Новосибирск";
$i = 0;
$jsondata = file_get_contents("http://rabota.ngs.ru/api/v1/vacancies/"); // Reads entire file into a string
$json = json_decode($jsondata, true); // Decodes a JSON string

$output = "<ul>";

foreach ($json["vacancies"] as $vacan) {
    $output .= "<h4>" . $vacan['header'] . "</h4>";
    $output .= "<li>id: " . $vacan['id'] . "</li>";
    $output .= "<li>Min salary = " . $vacan['salary_min'] . "</li>";
    $output .= "<li>Max salary = " . $vacan['salary_max'] . "</li>";
    $output .= "<li>Working type: " . $vacan['working_type']['title'] . "</li>";
    $output .= "<li>City: " . $vacan['contact']['city']['title'] . "</li>";

    $vacan['add_date'];
    list($date, $time) = split('[T]', $vacan['add_date']);
    $output .= "<li>date: " . $date . "</li>";

    // Foreach for array into array (rubrics)

    foreach ($vacan["rubrics"] as $vacancy) {
        $output .= "<li> Rubric: " . $vacancy['title'] . "</li>";
        $rubric[$i] = $vacancy['title'];
        $i++;
    }
}
$output .= "</ul>";
echo $output;
